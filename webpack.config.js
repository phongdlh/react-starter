/* eslint-disable  */
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");
const lodash = require("lodash");
const path = require("path");

function getAppConfig(env) {
  let data = require(`./src/configs/${env}`);

  return data;
}

module.exports = env => {
  const NODE_ENV = (env && env.NODE_ENV) || "development";
  process.env.NODE_ENV = NODE_ENV;

  console.log("Node ENV: %s", NODE_ENV);

  return {
    entry: path.resolve(__dirname, "src"),
    output: {
      filename: "[name][hash].js",
      path: path.resolve(__dirname, "build"),
      publicPath: getAppConfig(NODE_ENV).PUBLIC_PATH,
      chunkFilename: "[name][hash].js"
    },
    optimization: {
      splitChunks: {
        chunks: "all"
      }
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: ["babel-loader"]
        },
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
            "postcss-loader",
            "sass-loader"
          ]
        },
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader"]
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader",
              options: {
                minimize: true,
                root: path.resolve(__dirname, "src")
              }
            }
          ]
        },
        {
          test: /\.svg$/,
          use: [
            {
              loader: "babel-loader"
            },
            {
              loader: "react-svg-loader",
              options: {
                jsx: true
              }
            }
          ]
        },
        {
          test: /\.(jpg|jpeg|png|woff|eot|ttf|otf|pdf)$/,
          use: ["file-loader"]
        },
        {
          test: /\.ts(x?)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: "ts-loader"
            }
          ]
        },
        {
          enforce: "pre",
          test: /\.js$/,
          loader: "source-map-loader"
        }
      ]
    },
    plugins: [
      new CopyPlugin([{ from: "./src/resources", to: "./resources" }]),
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, "src/index.ejs")
      }),
      // new FaviconsWebpackPlugin({
      //   logo: path.resolve(__dirname, "src/resources/images/favicon.png"),
      //   icons: {
      //     android: false,
      //     appleIcon: false,
      //     appleStartup: false,
      //     coast: false,
      //     favicons: true,
      //     firefox: true,
      //     opengraph: false,
      //     twitter: false,
      //     yandex: false,
      //     windows: false
      //   }
      // }),
      new MiniCssExtractPlugin(),
      new webpack.DefinePlugin({
        "window._CONFIG": JSON.stringify(getAppConfig(NODE_ENV))
      })
    ],
    stats: "minimal",
    mode: "production",
    resolve: {
      extensions: [".js", ".jsx", ".ts", ".tsx"],
      alias: {
        src: path.join(__dirname, "src")
      }
    }
  };
};
