export default {
  HOME: {
    path: "/"
  },
  NOT_FOUND: {
    path: "/not-found"
  },
  SIGN_IN: {
    path: "/sign-in"
  }
};
