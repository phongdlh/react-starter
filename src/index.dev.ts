import "./app";

if (module.hot) {
  module.hot.accept("./app", () => {
    import("./app");
  });
}
