import Configs from "src/configs";
import Request from "./request";

const endpoint = `${Configs.API_URL}/`;
const apiKey = Configs.API_KEY;

const MainApi = new Request({
  endpoint,
  apiKey,
  handleToken: true
});

const PublicApi = new Request({
  endpoint,
  apiKey
});

export { PublicApi, MainApi };
