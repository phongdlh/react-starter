import axios from "axios";
import qs from "qs";
import Storage from "src/utils/storage";

class Request {
  _options: { endpoint: string; handleToken: any; apiKey: any };

  constructor(options) {
    this._options = options;
  }

  get(url: string, params: any, headers: any) {
    return this.request({ method: "GET", url, params, headers });
  }

  post(url: string, data: any, headers: any) {
    return this.request({ method: "POST", url, data, headers });
  }

  put(url: string, data: any, headers: any) {
    return this.request({ method: "PUT", url, data, headers });
  }

  delete(url: string, data: any, headers: any) {
    return this.request({ method: "DELETE", url, data, headers });
  }

  request({
    method,
    url,
    data,
    params,
    headers
  }: {
    method: any;
    url: string;
    data?: any;
    params?: any;
    headers: any;
  }) {
    headers = headers || {};
    url = this._options.endpoint + url;

    if (this._options.handleToken) {
      const token = Storage.get("ACCESS_TOKEN");

      if (token) {
        headers.Authorization = token;
      }
    }

    if (this._options.apiKey) {
      headers.ApiKey = this._options.apiKey;
    }

    return axios({
      method,
      url,
      data,
      headers,
      params,
      paramsSerializer: value => qs.stringify(value, { arrayFormat: "repeat" })
    });
  }
}

export default Request;
