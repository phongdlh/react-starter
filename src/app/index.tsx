import "@babel/polyfill";
import React from "react";
import ReactDOM from "react-dom";
import "src/resources/styles";
import Store from "src/store";
import Routes from "./routes";

function App() {
  return (
    <Store>
      <Routes />
    </Store>
  );
}

ReactDOM.render(<App />, document.getElementById("application"));
