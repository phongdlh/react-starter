import React, { Component, lazy, Suspense } from "react";
import { connect } from "react-redux";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import { compose } from "redux";
import RouterPath from "src/constants/router-path";
import Storage from "src/utils/storage";

const Home = lazy(() => import("src/pages/home"));
const SignIn = lazy(() => import("src/pages/sign-in"));
const NotFound = lazy(() => import("src/pages/not-found"));

const PrivateRoute = ({ condition, redirect, ...props }) => {
  condition = condition();

  if (condition) {
    return <Route {...props} />;
  }

  return <Redirect to={redirect} />;
};

const Loading = () => <h1>Loading</h1>;
class Routes extends Component {
  constructor(props) {
    super(props);
  }

  _authCondition = () => !Storage.has("ACCESS_TOKEN");

  _notAuthCondition = () => !Storage.has("ACCESS_TOKEN");

  _renderAuthContent = () => (
    <Switch>
      <Route exact path={RouterPath.HOME.path} component={Home} />
      <Redirect to={RouterPath.NOT_FOUND.path} />
    </Switch>
  );

  _renderAuthRoutes = () => <>{this._renderAuthContent()}</>;

  render() {
    return (
      <Suspense fallback={<Loading />}>
        <Switch>
          <PrivateRoute
            exact
            path={RouterPath.SIGN_IN.path}
            component={SignIn}
            condition={this._notAuthCondition}
            redirect={RouterPath.HOME.path}
          />
          <Route path={RouterPath.NOT_FOUND.path} component={NotFound} />
          <PrivateRoute
            path="/"
            component={this._renderAuthRoutes}
            condition={this._authCondition}
            redirect={RouterPath.SIGN_IN.path}
          />
        </Switch>
      </Suspense>
    );
  }
}

export default compose(withRouter)(Routes);
