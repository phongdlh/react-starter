declare global {
  interface Window {
    _CONFIG: any;
  }
}

const Config = { ...window._CONFIG };

export default Config;
