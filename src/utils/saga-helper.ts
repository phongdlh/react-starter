import { put } from "redux-saga/effects";
import { TYPES } from "src/store/actions";

export default function sagaHelper({ api, successMessage, errorMessage, out }) {
  return function*({ type, data, callback }) {
    const successType = `${type}_SUCCESS`;
    const failureType = `${type}_FAILURE`;

    try {
      yield put({ type: `${type}_REQUEST`, payload: data });
      const response = yield api(data);

      if (out) {
        yield put({ type: successType, data: response.data, payload: data });
        if (callback) callback(successType, response.data);
        return;
      }
      const { code, data: result, message } = response.data;

      if (code === 200) {
        yield put({ type: successType, data: result, payload: data });

        if (successMessage || message) {
          yield put({
            type: TYPES.ENQUEUE_SNACKBAR,
            data: {
              message: successMessage || message,
              options: {
                key: new Date().getTime() + Math.random(),
                variant: "success"
              }
            }
          });
        }

        if (callback) callback(successType, response.data);
      } else {
        yield put({ type: failureType, error: result });
        if (result?.name) {
          yield put({
            type: TYPES.ENQUEUE_SNACKBAR,
            data: {
              message: result?.name,
              options: {
                key: new Date().getTime() + Math.random(),
                variant: "error"
              }
            }
          });
        }

        if (callback) callback(failureType, response.data);
      }
    } catch (e) {
      const dataError = e?.response?.data;
      let message = dataError ? dataError.message : e.message;
      if (typeof message === "object") {
        message = Object.values(dataError.message).join(", ");
      }
      yield put({ type: failureType, error: errorMessage || message });
      if (errorMessage || message) {
        yield put({
          type: TYPES.ENQUEUE_SNACKBAR,
          data: {
            message: errorMessage || message,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: "error"
            }
          }
        });
      }

      if (dataError && dataError.status === 401) {
        yield put({ type: TYPES.SIGN_OUT });
      }

      if (callback) callback(failureType, errorMessage || dataError);
    } finally {
    }
  };
}
