# Project title

### Environment requirement

- Operating System: Ubuntu v18.04
- NodeJS: >= 12
- NPM: >= 6 or Yarn: >= 1.21

### Install Dependencies

```npm install```

```yarn install```

### Update env variable

- At path `./src/configs`, update each file for each environment
- Refer to file `./src/configs/local.json`

### Development (For Developer)

- Run:

  ```npm start```

  ```yarn start```

- Open browser at: `http://localhost:8080`
- Please install flug-in ESLINT into your IDE for code convention auto checking

### Develop Build

- Run: `npm run build:development`
- Copy all content in `./build` for deployment

### Staging Build

- Run: `npm run build:staging`
- Copy all content in `./build` for deployment

### Production Build

- Run: `npm run build`
- Copy all content in `./build` for deployment

### Check Convention

- Javascript: `npm run lint`
- CSS: `npm run stylelint`
